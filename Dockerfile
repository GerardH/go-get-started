FROM golang:1.12.8 AS builder
WORKDIR /workdir/
COPY ./ ./
#Build the webserver.
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o webserver .

FROM scratch
COPY --from=builder /workdir/webserver .
CMD ["./webserver"]

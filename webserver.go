package main

import (
	"fmt"
	"net/http"
)

func handleTest(w http.ResponseWriter, r *http.Request) {
	fmt.Fprint(w, "Hello World")
}

func main() {
	http.HandleFunc("/test", handleTest)
	http.ListenAndServe(":8080", nil)
}

# Go een praktisch voorbeeld

## Go geschiedenis
Go is ontwikkeld door Google werknemers die gefrustreerd waren door de onnodige complexiteit van talen als C, C++ en Java. Computers waren wel veel sneller geworden maar de manier van software ontwikkelen bleef achter. Dus besloten ze een stap terug te nemen en te kijken naar welke uitdagingen software ontwikkeling er in de komende jaren bij zou krijgen. Een voorbeeld hiervan was de opkomst van de multi-core CPU. De nieuwe taal die ze wilden ontwikkelen moest in hun ogen dus first class support hebben voor concurrency of parallellisme. En om resource management makkelijker te maken zou er een garbage collector moeten zijn of op zijn minst een manier om automatisch safe memory management te hebben. Dit proces begon 21 September 2007 met doelen op een whiteboard. In 2008 werd de taal verder ontwikkeld waarna het op 10 November 2009 een publiek open source [project](https://go.googlesource.com/go) werd. Voor meer informatie zie [hier](https://talks.golang.org/2012/splash.article)

## Hallo Playground
Na die korte introductie is het tijd om wat praktische dingen te doen. Wat we natuurlijk als eerste doen is een "hello world" programma schrijven. Dit kunnen we gewoon online doen op de [Go Playground](https://play.golang.org/). Hier word je al begroet door een "Hello playground" voorbeeld. Je kunt in de bovenste balk op play drukken om je code uit te proberen.
```go
package main

import (
	"fmt"
)

func main() {
	fmt.Println("Hello, playground")
}
```
Laten we nu eens spelen met variabelen. We kunnen als volgt een variable aanmaken.
```go
var a string = "a string"
fmt.Println(a)
```
Je kunt ook meerdere variabelen declareren.
```go
var b, c int = 1, 2
fmt.Println(b, c)
```
Go herkent het type dat je wilt opslaan in de variable dus je hoeft het niet te specificeren.
```go
var d = true
fmt.Println(d)
```
Je kunt het korter schrijven door ```:=``` te gebruiken. Hiermee declareer en initialiseer je de variable. Dus in plaats van ```var e string = "Shorthand"``` kunnen we ```e := "Shorthand"``` typen.
```go
e := "Shorthand"
fmt.Println(e)
```
Dit was het voor de variabelen.Als je meer wilt weten kun je de [tour of go](https://tour.golang.org/welcome/1) doen.

## Werken met Docker
Laten we een web API maken die in een docker container draait. Hiervoor hebben we in ieder geval [docker](https://docs.docker.com/install/) nodig maar het kan handig zijn om Go zelf te [installeren](https://golang.org/doc/install#install).

Om te beginnen maken we een bestand aan genaamd ```webserver.go```.
###### webserver.go
```go
package main

import (
	"fmt"
	"net/http"
)

func handleTest(w http.ResponseWriter, r *http.Request) {
	fmt.Fprint(w, "Hello World")
}

func main() {
	http.HandleFunc("/test", handleTest)
	http.ListenAndServe(":8080", nil)
}
```

#### Lokaal:
Als je Go geïnstalleerd hebt kun je met ```go run webserver.go``` de applicatie starten. Nu kun je naar http://localhost:8080/test gaan om te kijken of je API draait. Met ```go build -o webserver .``` bouwen we de executable.

#### Docker
Als je de applicatie met Docker wilt opstarten hebben we een ```Dockerfile``` nodig.
###### Dockerfile
```
FROM golang:1.12.8 AS builder
WORKDIR /workdir/
COPY ./ ./
#Build the webserver.
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o webserver .

FROM scratch
COPY --from=builder /workdir/webserver .
CMD ["./webserver"]
```
Deze kunnen we bouwen met behulp van het volgende commando ```docker build --rm -t test/webserver:test .```. Deze kunnen we daarna uitvoeren met ```docker run -p8080:8080 test/webserver:test```. Nu kun je naar http://localhost:8080/test gaan om te kijken of het werkt. Als het goed is zie je nu 'hello world' tevoorschijn komen.  Met ```docker images``` kunnen we de grootte van onze images bekijken. Hier kunnen we zien dat de image die we net gemaakt hebben 7.32MB is.

### Waarom Go
Ik vind go een mooie taal om mee te werken omdat het een duidelijke syntax heeft. Geen over complexe features heeft waardoor ik meer kan nadenken over het probleem dat ik oplos dan over de implementatie ervan. Verder vind ik het prettig dat de tooling van Go ook erg makkelijk is. Zoals ```go test``` voor het testen van de code, ```gofmt``` voor het formatteren van de code. Deze worden met de installatie van Go mee geleverd dus geen third party dingen zoals Java en Junit.

### Voor nog meer informatie
[Go FAQ](https://golang.org/doc/faq)  
[Documentation](https://golang.org/doc/)
